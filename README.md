# Programming tests with Spreedix Analytics

## Instruction
This is a test that mainly focus on basics of NLP.

A text file which includes labelled sentences is provided. It contains two labels 0(negative) and 1(positive). Please perform a sentiment analysis on this dataset. It is required to present your thoughts about pre-processing, model construction, training, testing, post-processing in your codes and comments. There is no training and testing separation provided. You may decide the best setting you think which will contribute to the performance of your classifier. You can use basic Python libraries as well as packages like scikit-learn, TensorFlow, PyTorch, Keras, SpaCy, Stanza, NLTK. Finally, please present your classification performance in terms of accuracy, f-score.

Before you work on the the task, please create a separate branch of your desired name from master branch. Then you may proceed to the task from your created branch. You may commit any work you have done anytime before the deadline to your created branch. But please do not commit to the master branch.

## Submission
Commit your final code to your created branch. And please create a separate file (requirements.txt) which documents all required packages or libraries we will need to test your solutions.
